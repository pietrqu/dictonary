<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class ConfigTable
 * @package AppBundle\Entity
 *          @ORM\Entity(repositoryClass="AppBundle\Repository\ConfigTableRepository")
 *          @ORM\Table(name="config_table")
 */
class ConfigTable
{
		/**
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 * @ORM\Column(type="integer")
		 */protected $id;
		/**
		 * @ORM\Column(type="string")
		 */protected $dictonaryOne;
		/**
		 * @ORM\Column(type="string")
		 */protected $dictonaryTwo;
		
		/**
		 * @return mixed
		 */
		public function getDictonaryOne()
		{
				return $this->dictonaryOne;
		}
		
		/**
		 * @param mixed $dictonaryOne
		 */
		public function setDictonaryOne($dictonaryOne)
		{
				$this->dictonaryOne = $dictonaryOne;
		}
		
		/**
		 * @return mixed
		 */
		public function getDictonaryTwo()
		{
				return $this->dictonaryTwo;
		}
		
		/**
		 * @param mixed $dictonaryTwo
		 */
		public function setDictonaryTwo($dictonaryTwo)
		{
				$this->dictonaryTwo = $dictonaryTwo;
		}
		
		/**
		 * @return mixed
		 */
		public function getId()
		{
				return $this->id;
		}

		
}
    