<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ConfigTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
		/**
		 * @Route("/", name="homepage")
		 */
		public function indexAction(Request $request)
		{
				
				$em = $this->getDoctrine()->getManager();
				$data = $em->getRepository(ConfigTable::class)
					->findAllOrdered();
				
				
				return $this->render(
					'default/index.html.twig',
					[
						'data' => $data,
					]
				);
		}
	
}
