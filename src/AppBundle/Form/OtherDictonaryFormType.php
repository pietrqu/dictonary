<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OtherDictonaryFormType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options)
		{
				
		}
		
		public function configureOptions(OptionsResolver $resolver)
		{
				$resolver->setDefaults(
					[
						'data_class' => 'AppBundle\Entity\OtherDictonary',
					]
				);
		}
		
		public function getBlockPrefix()
		{
				return 'app_bundle_other_dictonary_form_type';
		}
}
