<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class DictonaryOne
 * @package AppBundle\Entity
 *          @ORM\Entity(repositoryClass="AppBundle\Repository\DictonaryOneRepository")
 *          @ORM\Table(name="dictonary_one")
 */
class DictonaryOne
{
		/**
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 * @ORM\Column(type="integer")
		 */protected $id;
		/**
		 * @var string
		 * @ORM\Column(type="string")
		 */
		protected $name;
		/**
		 * @var string
		 * @ORM\Column(type="string")
		 */
		protected $description;
		/**
		 * @var boolean
		 * @ORM\Column(type="boolean")
		 */
		protected $isActive;
		/**
		 * @var int
		 * @ORM\Column(type="integer")
		 */
		protected $orderField;
		
		/**
		 * @return string
		 */
		public function getName()
		{
				return $this->name;
		}
		
		/**
		 * @param string $name
		 */
		public function setName($name)
		{
				$this->name = $name;
		}
		
		/**
		 * @return string
		 */
		public function getDescription()
		{
				return $this->description;
		}
		
		/**
		 * @param string $description
		 */
		public function setDescription($description)
		{
				$this->description = $description;
		}
		
		/**
		 * @return bool
		 */
		public function isActive()
		{
				return $this->isActive;
		}
		
		/**
		 * @param bool $isActive
		 */
		public function setIsActive($isActive)
		{
				$this->isActive = $isActive;
		}
		
		/**
		 * @return int
		 */
		public function getOrderField()
		{
				return $this->orderField;
		}
		
		/**
		 * @param int $orderField
		 */
		public function setOrderField($orderField)
		{
				$this->orderField = $orderField;
		}
		
		/**
		 * @return mixed
		 */
		public function getId()
		{
				return $this->id;
		}
		
		
}
    