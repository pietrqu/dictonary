<?php


namespace AppBundle\DataFixtures;


use AppBundle\Entity\ConfigTable;
use AppBundle\Entity\DictonaryOne;
use AppBundle\Entity\OtherDictonary;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
		public function load(ObjectManager $manager)
		{
				for ($i = 0 ; $i < 20 ; $i++) {
						
						$dictonaryOne = new DictonaryOne();
						$dictonaryOne->setDescription('d-one-val '.$i);
						$dictonaryOne->setIsActive(1);
						$dictonaryOne->setName('nameD '.$i);
						$dictonaryOne->setOrderField($i);
						
						$otherDictonary = new otherDictonary();
						$otherDictonary->setName('other-dict-val '.$i);
						$otherDictonary->setIsActive(1);
						$otherDictonary->setDescription('name Of OtherValue# '.$i);
						$otherDictonary->setOrderField($i);
						
						$ct = new ConfigTable();
						$ct->setDictonaryOne(rand(1,20));
						$ct->setDictonaryTwo(rand(1,20));
						
						$manager->persist($ct);
						$manager->persist($dictonaryOne);
						$manager->persist($otherDictonary);
				}
				
				$manager->flush();
		}
		
}
    