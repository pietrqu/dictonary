<?php


namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DictonaryTwoRepository extends EntityRepository
{
		public function findAllDictonaryTwoOrderedName()
		{
				return $this->getEntityManager()
					->createQuery(
						'SELECT d FROM AppBundle:DictonaryTwo d ORDER BY d.name'
					)
					->getResult();
		}
		
}
    