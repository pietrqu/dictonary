<?php


namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DictonaryOneRepository extends EntityRepository
{
		public function findAllOrderedByName()
		{
				return $this->getEntityManager()
					->createQuery(
						'SELECT d FROM AppBundle:DictonaryOne d ORDER BY d.name'
					)
					->getResult();
		}
		
}
    