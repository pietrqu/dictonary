<?php


namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ConfigTableRepository extends EntityRepository
{
		public function findAllOrdered()
		{
				return $this->getEntityManager()
					->createQuery(
						'SELECT c FROM AppBundle:ConfigTable c ORDER BY c.id'
					)
					->getResult();
		}
}
    