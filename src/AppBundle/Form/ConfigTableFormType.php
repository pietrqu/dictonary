<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigTableFormType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options)
		{
				$builder
					->add(
						'dictonaryOne',
						null,
						[
							'label' => 'label from dictonary 1',
						]
					)
					->add(
						'dictonaryTwo',
						null,
						[
							'label' => 'label from other dictonary ',
						]
					);
		}
		
		public function configureOptions(OptionsResolver $resolver)
		{
				$resolver->setDefaults(
					[
						'data_class' => 'AppBundle\Entity\ConfigTable',
					]
				);
		}
		
		public function getBlockPrefix()
		{
				return 'app_bundle_config_table_form_type';
		}
}
